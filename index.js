/**
 * Created by lukas on 5.11.15.
 */
'use strict';

module.exports = {
    getController: function () {

        var express = require('express');
        var mustacheExpress = require('mustache-express');

        var app = express();

        var rootPath = __dirname + '/../../../';

        app.engine('html', mustacheExpress());          // register file extension mustache
        app.set('view engine', 'html');                 // register file extension for partials

        app.set('views', __dirname + '/templates/admin');

        app.get('/', function(req, res) {
            res.render('master', {});
        });

        app.get('/login', function (req, res) {
            res.render('login', {});
        });

        return app;
    },

    loadDepencies: function () {

    }
}