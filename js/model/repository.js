var Entity = require('./entity');
require('../utils/utils');
var loginConfig = require('./loginConfig');

var Repository = can.Map.extend({
	name: null,
	apiUrl: null,
	init: function(name, endpoint) {
		endpoint = endpoint || false;
		this.name = name;
        if (endpoint) {
            this.apiUrl = name;
        } else {
            this.apiUrl = '/api/resource/'+name;
        }
	},

    makeRequest: function (method, url, data) {
        return can.ajax({
            url: this.apiUrl + url,
            type: method,
            async: true,
            dataType: 'json',
            data: data,
            contentType: "application/json; charset=utf-8",
            headers: {
                'x-access-token': loginConfig.getToken()
            }
        })
    },

	findOne: function(id) {
		var that = this;
		return this.makeRequest('GET', '/' + id)
            .then(function (data) {
                return new Entity(that, id, data);
            });
	},

	findAll: function(params) {
        var self = this;
        var params = params || {};

        return this.makeRequest('GET', '', params)
            .then(function(data) {
                var retData = [];
                for (var i in data) {
                    if (data.hasOwnProperty(i)) {
                        retData.push(new Entity(self, data[i].id, data[i]));
                    }
                }

                return retData;
            });
	},

	findTree: function(treeDefinition) {

		var rootTitle = typeof(treeDefinition.attr('root-title')) == 'undefined' ? 'root' : treeDefinition.attr('root-title');
		var tree = {
			title: [],
			groups: [],
			root: rootTitle
		};

		treeDefinition.find('title field').each(function() {
			tree.title.push($(this).attr('name'));
		});

		treeDefinition.find('groups group').each(function(key, val) {
			tree.groups.push($(this).attr());
		});

        return this.makeRequest('POST', '/tree', JSON.stringify(tree));
	},

	findRelated: function(field, value) {
		var that = this;

        return this.makeRequest('GET', '/related', {field: field, value: value})
            .then(function (data) {
                var retData = [];
                for (var i in data) {
                    retData.push(new Entity(that, data[i].id, data[i]));
                }

                return retData;
            });
	},

	findLike: function(fields, value) {
		var that = this;

        return this.makeRequest('GET', '/like', {fields: fields, value: value})
            .then(function (data) {
                var retData = [];
                for (var i in data) {
                    retData.push(new Entity(that, data[i].id, data[i]));
                }

                return retData;
            });
	},

	update: function(id, data) {
        var self = this;
        return this.makeRequest('PUT', '/' + id, JSON.stringify(data.attr()))
            .then(function(data) {
                return new Entity(self, data.id, data);
            });
	},

	delete: function(id) {

        return this.makeRequest('DELETE', '/' + id)
            .then(function(data) {
                return data;
            });
	},

	create: function(data) {
		var self = this;

        return this.makeRequest('POST', '', JSON.stringify(data.attr()))
            .then(function(data) {
                return new Entity(self, data.id, data);
            });
	},

	getEmptyEntity: function() {
		var entity = new Entity(this, 0, {});
		return entity;
	}
});

module.exports = Repository;
