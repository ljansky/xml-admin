/**
 * Created by lukas on 18.9.15.
 */

var LoginConfig = can.Map.extend({

    login: function (email, password) {
        var self = this;

        var data = {
            email: email,
            password: password
        };

        can.ajax({
            url: '/api/auth/authenticate',
            type: 'POST',
            async: true,
            dataType: 'json',
            data: data
        }).then(function(data) {
            if (data.ok) {
                self.setToken(data.token);
                self.dispatch('login', []);
            }
        });
    },

    logout: function () {
        var def = new can.Deferred();
        var self = this;
        var token = this.getToken();

        if (typeof token === 'undefined' || !token) {
            def.resolve(false);
        } else {
            can.ajax({
                url: '/api/auth/logout',
                type: 'POST',
                async: true,
                dataType: 'json',
                headers: {
                    'x-access-token': token
                }
            }).then(function(data) {
                if (data.ok) {
                    window.localStorage.removeItem('access_token');
                    self.dispatch('logout', []);
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }
            });
        }

        return def;
    },

    setToken: function (token) {
        window.localStorage.setItem('access_token', token);
    },

    getToken: function () {
        return window.localStorage.getItem('access_token');
    },

    isLogged: function () {
        var def = new can.Deferred();
        var token = this.getToken();
        if (typeof token === 'undefined' || !token) {
            def.resolve(false);
        } else {
            can.ajax({
                url: '/api/auth/logged',
                type: 'GET',
                async: true,
                dataType: 'json',
                headers: {
                    'x-access-token': token
                }
            }).then(function(data) {
                if (data.ok) {
                    def.resolve(true);
                } else {
                    def.resolve(false);
                }
            });
        }

        return def;
    }

});

can.extend(LoginConfig.prototype, can.event);

module.exports = new LoginConfig();