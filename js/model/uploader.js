/**
 * Created by lukas on 10.11.15.
 */

var can = require('can');
var loginConfig = require('./loginConfig');

var Uploader = function (targetUrl, directory) {
    this.targetUrl = targetUrl;
    this.directory = directory;
};

Uploader.prototype = {
    targetUrl: null,
    directory: null,

    upload: function(files) {
        var def = new can.Deferred();
        var formData = new FormData();

        var name = 'file';
        files.forEach(function(file) {
            formData.append(name, file);
        });

        formData.append('directory', this.directory);

        var request = new XMLHttpRequest();

        request.onreadystatechange = function() {
            console.log(request.readyState, request.status);
            if (request.readyState==4 && request.status==200) {
                var response = JSON.parse(request.responseText);
                def.resolve(response);
            }
        };

        request.open("POST", this.targetUrl);
        request.setRequestHeader('x-access-token', loginConfig.getToken());
        request.send(formData);

        return def;
    }
};

module.exports = Uploader;