require('can/map/delegate');

var Entity = can.Map.extend({
	repository: null,
	id: null,
	data: null,
	errors: new can.Map({}),
    modified: false,

	init: function(repository, id, data) {
        var self = this;
		this.repository = repository;
		this.id = id;
		this.attr('data', new can.Map(data));

        if (!id) {
            self.dispatch('entity-modified', [true]);
            self.attr('modified', true);
        }

        this.data.delegate('*','change', function() {
            self.dispatch('entity-modified', [true]);
            self.attr('modified', true);
        });
	},
	get: function(field) {
		return this.data.attr(field);
	},
	set: function(field, value) {
		this.data.attr(field, value);
	},
	setData: function(data) {
		var that = this;
		data.each(function(val, key) {
			that.set(key, val);
		});
	},
	save: function() {
        var self = this;
		return this.repository.update(this.id, this.data).then(function (data) {
            self.dispatch('entity-modified', [false]);
            return data;
        });
	},
	delete: function() {
		return this.repository.delete(this.id);
	},
	saveNew: function() {
        var self = this;
		return this.repository.create(this.data).then(function (data) {
            self.dispatch('entity-modified', [false]);
            return data;
        });
	},
	setErrors: function(errors) {
		for (var i in errors) {
			if (i != '_global') {
				this.errors.attr(i, errors[i]);
			}
		}
	},
	setModified: function (boolValue) {
		this.dispatch('entity-modified', [boolValue]);
		this.attr('modified', boolValue);
	}
});

can.extend(Entity.prototype, can.event);

module.exports = Entity;
