/**
 * Created by lukas on 22.5.15.
 */

'use strict';

var can = require('can');

var FieldFactory = require('../fields/fieldFactory');
var dialogFactory = require('../dialogs/dialogFactory');
var actionFactory = require('../actions/actionFactory');

var FormConfig = can.Map.extend({
    // static

}, {
    $element: null,
    fields: null,
    entity: null,
    initialized: null,
    actions: null,
    formId: null,
    repository: null,
    flashMessages: {},
    flashMessagesCounter: 0,

    init: function (config, entity, repository, formId) {
        var self = this;
        this.attr('fields', new can.Map({}));
        this.attr('initialized', new can.Deferred());
        this.attr('entity', entity);
        this.attr('repository', repository);

        if (typeof formId !== 'undefined') {
            this.attr('formId', formId);
        }

        this.setActions(config);

        can.$(config).children().each(function (key, field) {
            var newField = FieldFactory.createField(field, self);
            self.fields.attr(newField.id, newField);
        });

        /**
         * TODO: toto nejak vyresit
         */
        setTimeout(function () {
            self.initFields();
        }, 500);


        this.initialized.resolve();
    },

    setCustomActions: function (actions) {
        for (var i in actions) {
            if (actions.hasOwnProperty(i)) {
                this.actions[i] = actions[i];
            }
        }
    },

    setAction: function (name) {
        if (typeof this.actions[name] === 'undefined') {
            var act = actionFactory.getAction(name);
            if (act) {
                this.actions[name] = act;
            }
        }
    },

    setActions: function (config) {
        this.attr('actions', {});
        var self = this;
        $(config).find('actions button').each(function (key, action) {
            var actionName = $(action).attr('callback');
            self.setAction(actionName);
        });
    },

    setData: function (entity) {
        this.attr('entity', entity);
        this.initValues();
        this.clearErrors();
    },

    initFields: function () {
        this.fields.each(function (field) {
            if (typeof field.initField === 'function') {
                field.initField();
            }
        });
    },

    initValues: function () {
        this.fields.each(function (field) {
            if (typeof field.initValue === 'function') {
                field.initValue();
            }
        });
    },

    getFieldsPath: function () {
        return 'config.fields.';
    },

    setFlashMessage: function (message) {
        var self = this;
        var flashMessagesCounter = this.flashMessagesCounter;
        this.flashMessages.attr(flashMessagesCounter, message);
        setTimeout(function () {
            self.flashMessages.removeAttr(flashMessagesCounter);
        }, 1500);
        this.flashMessagesCounter++;
    },

    setErrors: function (errors) {
        errors = errors || [];
        for (var i = 0; i < errors.length; i++) {
            if (errors[i].field) {
                this.setError(errors[i].field, errors[i].message);
            } else {
                this.setGlobalError(errors[i].message);
            }
        }
    },

    clearErrors: function () {
        this.fields.each(function (field) {
            if (typeof field.setError === 'function') {
                field.setError(null, null);
            }
        });
    },

    setError: function (fieldName, message) {
        this.fields.each(function (field) {
            if (typeof field.setError === 'function') {
                field.setError(fieldName, message);
            }
        });
    },

    setGlobalError: function (message) {
        dialogFactory.showDialog('alert', {
            message: message
        });
    },

    setDefaultValues: function () {
        this.fields.each(function (field) {
            field.setDefaultValue();
        });
    },

    callAction: function (name, closeAfter) {
        var self = this;
        var callback = this.actions[name];
        if (typeof callback === 'function') {

            this.clearErrors();
            return callback.apply(this, [closeAfter]).fail(function (err) {
                self.setErrors(err.responseJSON);
            });
        }
    },

    getFormConfig: function () {
        return this;
    }
});

can.extend(FormConfig.prototype, can.event);

window.FormConfig = FormConfig;

module.exports = FormConfig;