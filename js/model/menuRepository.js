var Entity = require('./entity');
require('../utils/utils');
var loginConfig = require('./loginConfig');

var MenuRepository = can.Map.extend({
    name: null,
    apiUrl: null,
    init: function() {
        this.name = 'Menu';
        this.apiUrl = '/api/menu'
    },

    makeRequest: function (method, url, data) {
        return can.ajax({
            url: this.apiUrl + url,
            type: method,
            async: true,
            dataType: 'json',
            data: data,
            contentType: "application/json; charset=utf-8",
            headers: {
                'x-access-token': loginConfig.getToken()
            }
        })
    },

    findAll: function(params) {
        var params = params || {};

        return this.makeRequest('GET', '', params)
            .then(function(data) {
                return data;
            });
    }
});

module.exports = MenuRepository;