
var loginConfig = require('../model/loginConfig');

var BaseController = can.Control({
    moduleDef: can.compute(null),
    FormConfig: null,
    customActions: {},

    init: function(el, options) {
        var self = this;
        this.FormConfig = options.FormConfig;
        loginConfig.on('login', function () {
            self.element.html(can.view('/js/xml-admin/templates/base.mustache', {form: self.moduleDef}));
        });

        loginConfig.on('logout', function () {
            self.element.html(can.view('/js/xml-admin/templates/login.mustache', {}));
        });

        loginConfig.isLogged().then(function (logged) {
            if (logged) {
                self.element.html(can.view('/js/xml-admin/templates/base.mustache', {form: self.moduleDef}));
            } else {
                self.element.html(can.view('/js/xml-admin/templates/login.mustache', {}));
            }
        });
    },

    ':module/:id route': function(data) {
        var self = this;
        $.ajax({
            url: '/xml/'+data.module+'.xml',
            type: 'get',
            dataType: 'xml'
        }).done(function(data) {
            var moduleDef = $('module', data);
            self.setModule(moduleDef);
        });
    },

    'logout route': function () {
        loginConfig.logout();
    },

    setCustomActions: function (actions) {
        this.customActions = actions;
    },

    setModule: function(moduleDef) {
        var type = moduleDef.attr('type');
        var template = can.mustache('<' + type + '-module module={def} custom-actions={customActions}></' + type + '-module>');
        var content = template({
            def: moduleDef,
            customActions: this.customActions
        });
        $('#module-part').html(content);
    }
});

module.exports = BaseController;
