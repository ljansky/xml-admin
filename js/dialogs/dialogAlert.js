/**
 * Created by lukas on 19.10.15.
 */

var showDialog = function (data) {
    var deferred = new can.Deferred();
    this.makeDialog('alert', data)
        .on('hidden.bs.modal', function() {
            deferred.resolve();
        });

    return deferred;
};

module.exports = showDialog;