/**
 * Created by lukas on 19.10.15.
 */

var dialogAlert = require('./dialogAlert');
var dialogForm = require('./dialogForm');
var dialogPrompt = require('./dialogPrompt');

var dialogFactory = {

    dialogCount: 0,

    showDialog: function (name, options) {
        options.dialogId = 'dialog' + this.dialogCount;
        this.dialogCount++;

        switch (name) {
            case 'alert':
                return dialogAlert.apply(this, [options]);
            case 'form':
                return dialogForm.apply(this, [options]);
            case 'prompt':
                return dialogPrompt.apply(this, [options]);
        }
    },

    makeDialog: function (template, data) {
        var dialog = $(can.view('/js/xml-admin/templates/dialogs/' + template + '.mustache', data));
        $('body').append(dialog);

        var modal = $('#' + data.dialogId)
            .modal({ backdrop: 'static', keyboard: false })
            .on('hidden.bs.modal', function() {
                $('#' + data.dialogId).remove();
                $('.modal:visible').length && $(document.body).addClass('modal-open');
            });

        modal.closeDialog = function () {
            $('#' + data.dialogId).modal('hide');
        };

        return modal;
    },

    makeCustomDialog: function (template, data) {
        this.dialogCount++;
        data.dialogId = 'dialog' + this.dialogCount;
        var dialog = $(can.view('/templates/dialogs/' + template + '.mustache', data));
        $('body').append(dialog);

        var modal = $('#' + data.dialogId)
            .modal({ backdrop: 'static', keyboard: false })
            .on('shown.bs.modal', function () {
                $('.modal:visible').length && $(document.body).addClass('modal-open');
            })
            .on('hidden.bs.modal', function () {
                $('#' + data.dialogId).remove();
                $('.modal:visible').length && $(document.body).addClass('modal-open');
            });

        modal.closeDialog = function () {
            $('#' + data.dialogId).modal('hide');
        };

        return modal;
    },

    closeDialog: function (dialogId) {
        $('#' + dialogId).modal('hide');
    }
};

module.exports = dialogFactory;
