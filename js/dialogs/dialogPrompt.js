/**
 * Created by lukas on 19.10.15.
 */

var showDialog = function (data) {
    var self = this;
    var deferred = new can.Deferred();
    this.makeDialog('prompt', data)
        .on('click', '.confirm', function () {
            deferred.resolve(true);
            self.closeDialog(data.dialogId)
        })
        .on('hidden.bs.modal', function() {
            deferred.resolve(false);
        });

    return deferred;
};

module.exports = showDialog;