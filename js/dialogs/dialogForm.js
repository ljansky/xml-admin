/**
 * Created by lukas on 19.10.15.
 */

var showDialog = function (data) {
    data.entityId = data.entityId || 0;
    var self = this;
    return can.ajax({
        url: '/xml/' + data.module + '.xml',
        type: 'get',
        dataType: 'xml'
    }).then(function (xml) {
        var deferred = new can.Deferred();
        var module = $('module', xml);

        var formConfig = new window.FormConfig(module.find('form')[0], null, data.repository, data.dialogId);

        if (typeof data.sourceForm !== 'undefined') {
            var actions = {};
            for (var i in data.sourceForm.actions) {
                if (data.sourceForm.actions.hasOwnProperty(i) && typeof formConfig.actions[i] === 'undefined') {
                    actions[i] = data.sourceForm.actions[i];
                }
            }

            formConfig.setCustomActions(actions);
        }

        formConfig.on('saved', function(ev, entity) {
            deferred.resolve(entity);
        });

        self.makeDialog('form', {config: formConfig, dialogId: data.dialogId})
            .on('hidden.bs.modal', function() {
                deferred.resolve(null);
            }).on('shown.bs.modal', function() {
                formConfig.setData(data.entity);
            });

        return deferred;
    });
};

module.exports = showDialog;