/**
 * Created by lukas on 14.9.15.
 */
var can = require('can');
require('can/construct/super');

var Rule = can.Map.extend({

    config: null,

    init: function (config, parent) {
        this.attr('config', can.$(config));
        this.attr('parent', parent);
    },

    getMessage: function () {
        return this.config.attr('message');
    }
});

module.exports = Rule;