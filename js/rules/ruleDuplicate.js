/**
 * Created by lukas on 7.9.15.
 */

var Rule = require('./rule');

var RuleDuplicate = Rule.extend({

    fields: [],

    init: function (config, parent) {
        this._super(config, parent);
        var self = this;

        this.config.children().each(function (key, field) {
            self.fields.push($(field).attr('name'));
        });
    },

    getError: function () {
        var self = this;
        var deferred = new can.Deferred();

        var fields = {};

        for (var i = 0; i < this.fields.length; i++) {
            fields[this.fields[i]] = this.parent.entity.get(this.fields[i]);
        }

        $.ajax({
            method: 'GET',
            url: '/api/validate/duplicate',
            data: {
                repository: this.config.attr('repository'),
                fields: fields,
                id: this.parent.entity.get('id')
            }
        }).then(function (res) {
            if (res.ok) {
                deferred.resolve();
            } else {
                deferred.resolve(self.getMessage());
            }
        })

        return deferred;
    }
});

module.exports = RuleDuplicate;