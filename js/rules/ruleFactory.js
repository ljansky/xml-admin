/**
 * Created by lukas on 7.9.15.
 */
'use strict';

var RuleDuplicate = require('./ruleDuplicate');

var ruleFactory = {
    createRule: function (config, parent) {
        var type = can.$(config).attr('type');

        switch (type) {
            case 'duplicate':
                return new RuleDuplicate(config, parent);
        }
    }
};

module.exports = ruleFactory;