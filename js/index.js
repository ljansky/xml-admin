/**
 * Created by lukas on 5.11.15.
 */
require('xml-admin/components/mainMenu');
require('xml-admin/components/baseModule');
require('xml-admin/components/baseForm');
require('xml-admin/components/loginForm');
require('xml-admin/components/entityTree');
require('xml-admin/components/entityTreeItem');
require('bootstrap');

require('bootstrap-css/bootstrap.css!');
require('bootstrap-datepicker-css/bootstrap-datepicker3.min.css!');
require('xml-admin-css/layout.css!');

var BaseController = require('xml-admin/controllers/baseController');
var FormConfig = require('xml-admin/model/formConfig');

var baseController = null;

module.exports = {

    getBaseController: function (elementId) {
        if (!baseController) {
            baseController = new BaseController(elementId, {FormConfig: FormConfig});
        }

        return baseController
    }
}