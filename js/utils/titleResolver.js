/**
 * Created by lukas on 9.10.15.
 */

var TitleResolver = function (titleInfo) {
    this.titleInfo = titleInfo;
    this.fields = [];
    this.initFields();
    this.separator = ' ';
};

TitleResolver.prototype = {

    initFields: function () {
        var self = this;

        this.titleInfo.find('field').each(function() {
            self.fields.push({
                name: $(this).attr('name'),
                foreign: $(this).attr('foreign') || null,
                prefix: $(this).attr('prefix') || '',
                postfix: $(this).attr('postfix') || ''
            });
        });
    },

    getTitle: function (entity) {
        var parts = [];
        this.fields.forEach(function (field) {
            var part = '';
            if (field.foreign) {
                part = entity.get(field.foreign)[field.name];
            } else {
                part = entity.get(field.name);
            }

            if (part) {
                parts.push(field.prefix + part + field.postfix);
            }
        });

        return parts.join(this.separator);
    },

    getFields: function () {
        var ret = [];
        this.fields.forEach(function (field) {
            if (field.foreign) {

            } else {
                ret.push(field.name);
            }
        });
        return ret;
    },

    getFormat: function () {
        var format = {
            id: 1
        };

        this.fields.forEach(function (field) {
            if (field.foreign) {
                var subField = {};
                subField[field.name] = 1;
                format[field.foreign] = subField;
            } else {
                format[field.name] = 1;
            }
        });

        return format;
    }

};

module.exports = TitleResolver;
