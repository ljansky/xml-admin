/**
 * Created by lukas on 4.6.15.
 */

var Geocoder = can.Construct.extend({}, {
    getLatLngByAdress: function(address) {
        var geocoder = new google.maps.Geocoder();
        var deferred = new can.Deferred();

        geocoder.geocode( { 'address': address}, function(results, status) {
            console.log(results, status, address);
            if (google.maps.GeocoderStatus.OK) {
                var region = '';
                var i = 0;
                for (i in results) {
                    var j = 0;
                    for (j in results[i].address_components) {
                        if (results[i].address_components[j].types == 'administrative_area_level_1,political') {
                            region = results[i].address_components[j].short_name;
                            break;
                        }
                    }
                    if (region != '') {
                        break;
                    }
                }

                deferred.resolve(results[0].geometry.location);
            }
            else {
                deferred.reject("Pozici se nepodarilo najit.");
            }
        });

        return deferred;
    }
});

module.exports = Geocoder;