var MenuRepository = require('../model/menuRepository');

can.Component.extend({
    tag: "main-menu",
    viewModel: {
        items: null,

        init: function () {
            var self = this;

            this.attr('items', new can.List([]));
            var repository = new MenuRepository();
            repository.findAll().then(function(items) {
                for (var i = 0; i < items.length; i++) {
                    self.items.push(items[i]);
                }
                self.items.push({ link: 'logout', title: 'Odhlásit' });
            });
        }
    },
    template: can.view('/js/xml-admin/templates/mainMenu.mustache')
})