/**
 * Created by lukas on 31.7.15.
 */
/**
 * Created by lukas on 21.5.15.
 */
'use strict';

var can = require('can');

can.Component.extend({

    template: function (data, helpers) {

        var html = '<fieldset class="related-fieldset">';

        html += '<div class="related-item-items">';

        data.attr('config').fields.each(function (field) {
            html += field.getTemplate();
        });
        html += '</div>';

        html += '<div class="related-item-buttons">';
        html += '<button type="button" can-click="config.callAction \'save\'" class="btn btn-primary btn-xs">Uložit</button>';
        html += '<button type="button" can-click="config.callAction \'delete\'" class="btn btn-danger btn-xs">Smazat</button>';

        html += '<div class="messages">';
        html += '{{#each config.flashMessages}}<div class="alert alert-success" role="alert">{{.}}</div>{{/each}}';
        html += '</div>';

        html += '</div>';

        html += '</fieldset>';

        return can.mustache(html)(data, helpers);
    },

    tag: 'inline-form',

    viewModel: {

        $element: null,

        config: null
    },

    init: function (element) {
        this.viewModel.attr('$element', can.$(element));
    },

    events: {
        inserted: function () {
            this.viewModel.config.initFields();
            this.viewModel.config.initValues();
        }
    }
});