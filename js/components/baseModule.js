var can = require('can');
var Repository = require('../model/repository');
var FormConfig = require('../model/formConfig');

can.Component.extend({
    tag: "base-module",
    scope: {
    	element: null,
        tree: new can.Map({}),
        selectedItem: null,
        treeDefinition: null,
        formConfig: null,
        repository: null,
        module: null,
        customActions: {},

        loadTree: function() {
            var that = this;
            this.repository.findTree(this.treeDefinition).then(function(data) {
                that.attr('tree', new can.Map(data));
            });
        },
        loadEntity: function(id) {
            var self = this;
            this.repository.findOne(id).then(function(data) {
                self.formConfig.setData(data);
            });
        }
    },
    template: can.view('/js/xml-admin/templates/baseModule.mustache'),
    events: {
    	init: function(element) {
            var S = this.scope;
            S.element = element;

            S.attr('repository', new Repository(S.module.attr('repository')));
            S.attr('treeDefinition', S.module.find('tree'));
            var formConfig = new FormConfig(S.module.find('form')[0], S.repository.getEmptyEntity(), S.repository);
            formConfig.setCustomActions(S.customActions);

            S.attr('formConfig', formConfig);

            S.formConfig.initialized.then(function () {
                S.formConfig.on('saved', function() {
                    S.loadTree();
                });

                S.formConfig.on('deleted', function() {
                    S.loadTree();
                });
            });

            S.loadTree();
        },
        'base-form renderTree': function() {
            this.scope.loadTree();
        },
        'entity-tree-item entitySelected': function(element, event, data) {
            if (this.scope.selectedItem) {
                this.scope.selectedItem.attr('selected', false);
            }

            this.scope.selectedItem = data;
            this.scope.loadEntity(data.entityId);
        }
    }
  })
