/**
 * Created by lukas on 21.5.15.
 */
'use strict';

var can = require('can');

can.Component.extend({

    template: function (data, helpers) {
        var html = '<form class="base-form">';
        html += '<div class="messages">';
        html += '{{#each config.flashMessages}}<div class="alert alert-success" role="alert">{{.}}</div>{{/each}}';
        html += '</div>';
        data.attr('config').fields.each(function (field) {
            html += field.getTemplate();
        });
        html += '</form>';

        return can.mustache(html)(data, helpers);
    },

    tag: 'base-form',

    viewModel: {

        $element: null,

        config: null
    },

    events: {
        init: function (element) {
            this.viewModel.attr('$element', element);
            this.viewModel.config.attr('$element', element);
        }
    }
});
