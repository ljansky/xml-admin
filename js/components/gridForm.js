/**
 * Created by lukas on 28.9.15.
 */

'use strict';

var can = require('can');

can.Component.extend({

    template: function (data, helpers) {

        var html = '<fieldset class="grid-fieldset form-inline">';
        html += '<div class="form-group form-group-sm">';
        data.attr('config').fields.each(function (field) {
            html += '<div class="grid-item-wrap">';
            html += field.getTemplate();
            html += '</div>';
        });

        html += '<button type="button" can-click="config.callAction \'save\'" class="btn btn-primary btn-xs">Uložit</button>';

        html += '</div>';

        html += '</fieldset>';

        return can.mustache(html)(data, helpers);
    },

    tag: 'grid-form',

    viewModel: {

        $element: null,

        config: null
    },

    init: function (element) {
        this.viewModel.attr('$element', can.$(element));
    },

    events: {
        inserted: function () {
            this.viewModel.config.initFields();
            this.viewModel.config.initValues();
        }
    }
});