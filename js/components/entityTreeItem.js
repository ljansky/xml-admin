can.Component.extend({
    tag: "entity-tree-item",
    scope: {
        element: null,
        entityId: null,
        title: null,
        selected: false
    },
    template: can.view('/js/xml-admin/templates/entityTreeItem.mustache'),
    events: {
        init: function(element) {
            this.scope.element = element;
        },
        'a click': function() {
            this.scope.attr('selected', true);
            can.$(this.scope.element).trigger('entitySelected', this.scope);
            return false;
        }
    }
})