/**
 * Created by lukas on 17.9.15.
 */
'use strict';

var can = require('can');
var loginConfig = require('../model/loginConfig');

can.Component.extend({

    template: can.view('/js/xml-admin/templates/loginForm.mustache'),

    tag: 'login-form',

    viewModel: {

        $element: null,

        email: '',

        password: '',

        login: function () {
            loginConfig.login(this.email, this.password);
            return false;
        }
    },

    events: {
        init: function (element) {
            this.viewModel.attr('$element', element);
        }
    }
});