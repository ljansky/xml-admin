can.Component.extend({
    tag: "entity-tree",
    scope: {
        element: null,
        items: null,
        visible: false,
        toggleVisibility: function() {
            this.attr('visible', !this.visible);
            return false;
        }
    },
    template: can.view('/js/xml-admin/templates/entityTree.mustache')
})