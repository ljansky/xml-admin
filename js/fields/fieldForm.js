/**
 * Created by lukas on 22.5.15.
 */

'use strict';

var can = require('can');
var actionFactory = require('../actions/actionFactory');
require('can/map/delegate');
var dialogFactory = require('../dialogs/dialogFactory');

var FieldForm = can.Map.extend({
    // static

}, {
    fields: null,
    entity: null,
    actions: null,
    parent: null,
    modified: false,
    flashMessages: {},
    flashMessagesCounter: 0,

    init: function (config, entity, FieldFactory, parent) {
        var self = this;
        this.attr('fields', new can.Map({}));
        this.attr('entity', entity);

        this.attr('parent', parent);

        this.setActions(config);

        can.$(config).children().each(function (key, field) {
            var newField = FieldFactory.createField(field, self);
            self.fields.attr(newField.id, newField);
        });
    },

    setActions: function (config) {
        this.attr('actions', {});
        var self = this;

        var actions = ['save', 'delete'];

        actions.forEach(function (actionName) {
            self.actions[actionName] = actionFactory.getAction(actionName);
        });
    },

    initFields: function () {
        this.fields.each(function (field) {
            if (typeof field.initField === 'function') {
                field.initField();
            }
        });
    },

    initValues: function () {
        var self = this;

        if (this.entity) {
            this.attr('modified', this.entity.modified);
            this.entity.on('entity-modified', function(ev, modified) {
                self.attr('modified', modified);
            });
        }

        this.fields.each(function (field) {
            if (typeof field.initValue === 'function') {
                field.initValue();
            }
        });
    },

    getFieldsPath: function () {
        return 'config.fields.';
    },

    getFormConfig: function () {
        return this;
    },

    setFlashMessage: function (message) {
        var self = this;
        var flashMessagesCounter = this.flashMessagesCounter;
        this.flashMessages.attr(flashMessagesCounter, message);
        setTimeout(function () {
            self.flashMessages.removeAttr(flashMessagesCounter);
        }, 1500);
        this.flashMessagesCounter++;
    },

    setErrors: function (errors) {
        errors = errors || [];
        for (var i = 0; i < errors.length; i++) {
            if (errors[i].field) {
                this.setError(errors[i].field, errors[i].message);
            } else {
                this.setGlobalError(errors[i].message);
            }
        }
    },

    clearErrors: function () {
        this.fields.each(function (field) {
            if (typeof field.setError === 'function') {
                field.setError(null, null);
            }
        });
    },

    setError: function (fieldName, message) {
        this.fields.each(function (field) {
            if (typeof field.setError === 'function') {
                field.setError(fieldName, message);
            }
        });
    },

    setGlobalError: function (message) {
        dialogFactory.showDialog('alert', {
            message: message
        });
    },

    setData: function (entity) {
        this.attr('entity', entity);

        this.initValues();
        this.clearErrors();
    },

    callAction: function (name) {
        var self = this;
        var callback = this.actions[name];
        if (typeof callback === 'function') {
            this.clearErrors();
            callback.apply(this, []).then(function (res) {
                if (name === 'save' && res.insert) {
                    self.setData(res.entity);
                    self.parent.insertEmptyForm();
                }

                if (name === 'delete') {
                    self.parent.removeForm(res.id);
                }
            }).fail(function (err) {
                self.setErrors(err.responseJSON);
            });
        }
    }
});

can.extend(FieldForm.prototype, can.event);

module.exports = FieldForm;