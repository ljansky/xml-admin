/**
 * Created by lukas on 11.6.15.
 */

'use strict';

var can = require('can');
var FieldInput = require('./fieldInput');
var Repository = require('../model/repository');
var TitleResolver = require('../utils/titleResolver');

var FieldSelect = FieldInput.extend({
    options: null,
    value: null,
    titleResolver: null,

    init: function(config, parent) {
        var self = this;
        this._super(config, parent);
        this.attr('options', new can.List([]));
        this.titleResolver = new TitleResolver(this.config.find('title'));

        this.bind('value', function () {
            if (self.getFormConfig().entity) {
                self.getFormConfig().entity.set(self.name, self.value);
            }
        });
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        return '<select class="form-control" id="' + this.id + '" can-value="' + this.getTemplateAttr('value') + '">'
             + '{{#each ' + this.getTemplateAttr('options') + '}}<option value="{{value}}">{{title}}</option>{{/each}}'
             + '</select>';
    },

    setOptions: function(selectedValue) {
        var deferred = new can.Deferred();
        var order = [];
        this.config.find('order').each(function (key, ord) {
            var $ord = $(ord);
            order.push([$ord.attr('field'), $ord.attr('direction')]);
        });

        var self = this;

        var itemsDefinition = this.config.find('items');
        var repository = null;
        if (itemsDefinition.attr('repository')) {
            repository = new Repository(itemsDefinition.attr('repository'));
        }

        if (itemsDefinition.attr('endpoint')) {
            repository = new Repository(itemsDefinition.attr('endpoint').replace('{value}', selectedValue), true);
        }

        var options = [];

        this.config.find('static').each(function () {
            var opt = $(this);
            options.push({
                value: opt.attr('value'),
                title: opt.text()
            });
        });

        if (repository) {
            repository.findAll({
                order: order,
                format: this.titleResolver.getFormat()
            }).then(function(data) {
                options.push({
                    value: 0,
                    title: itemsDefinition.attr('default')
                });

                for (var i in data) {
                    options.push({
                        value: data[i].id,
                        title: self.titleResolver.getTitle(data[i])
                    });
                }
                self.options.replace(options);
                deferred.resolve();
            });
        } else {
            self.options.replace(options);
            deferred.resolve();
        }

        return deferred;
    },

    initValue: function() {
        var value = 0;
        var entity = this.getFormConfig().entity;
        if (entity) {
            value = entity.get(this.name) || 0;
            var self = this;
            this.setOptions(value)
            .then(function () {
                self.attr('value', value);
                setTimeout(function () {
                    if (entity.id) {
                        entity.setModified(false);
                    }
                }, 500);
            });
        }
    }
});

module.exports = FieldSelect;
