/**
 * Created by lukas on 31.7.15.
 */
'use strict';
var FieldContainer = require('./fieldContainer');

var FieldCard = FieldContainer.extend({
    active: false,

    getLabelTemplate: function () {
        return '{{#unless ' + this.getTemplateAttr('disabled') + '}}<li {{#if ' + this.getTemplateAttr('active') + '}}class="active"{{/if}}><a href="javascript:void(0)" can-click="{' + this.getTemplateAttr('choose') + '}">' + this.title + '</a></li>{{/unless}}';
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        var html = '<div class="card{{#if ' + this.getTemplateAttr('active') + '}} active{{/if}}">';
        this.fields.each(function (field) {
            html += field.getTemplate();
        });
        html += '</div>';

        return html;
    },

    choose: function () {
        this.parent.hideCards();
        this.attr('active', true);
        return false;
    }
});

module.exports = FieldCard;