/**
 * Created by lukas on 31.7.15.
 */

'use strict';
var FieldInput = require('./fieldInput');
var RuleFactory = require('../rules/ruleFactory');
var can = require('can');
var dialogFactory = require('../dialogs/dialogFactory');

var FieldButton = FieldInput.extend({
    callback: 'save',
    closeAfter: true,

    type: 'default',

    types: {
        'new': 'info',
        'save': 'primary',
        'delete': 'danger'
    },

    rules: null,

    init: function(config, parent) {
        var self = this;
        this._super(config, parent);
        this.attr('callback', can.$(config).attr('callback'));
        this.attr('closeAfter', can.$(config).attr('close-on-new') === 'false' ? false : true);
        if (typeof this.types[this.callback] !== 'undefined') {
            this.attr('type', this.types[this.callback]);
        }

        this.rules = new can.List([]);

        can.$(config).children().each(function (key, rule) {
            var newRule = RuleFactory.createRule(rule, self.getFormConfig());
            self.rules.push(newRule);
        });
    },

    getTemplate: function () {
        return this.getFieldTemplate();
    },

    click: function () {
        var self = this;
        var rulePromises = [];

        this.rules.each(function (rule) {
            rulePromises.push(rule.getError());
        });

        $.when.apply($, rulePromises).then(function(){
            var objects = arguments;

            var messages = [];

            for (var i = 0; i < objects.length; i++) {
                if (typeof objects[i] !== 'undefined' && objects[i] && objects[i] !== '') {
                    messages.push(objects[i]);
                }
            }

            return messages;
        }).then(function (messages) {
            if (messages.length > 0) {
                dialogFactory.showDialog('prompt', {
                    message: messages[0]
                }).then(function (res) {
                    if (res) {
                        self.getFormConfig().callAction(self.callback, self.closeAfter);
                    }
                });
            } else {
                self.getFormConfig().callAction(self.callback, self.closeAfter);
            }
        });
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        return '<button type="button" class="btn btn-' + this.type + '" id="' + this.id + '" can-click="{' + this.getTemplateAttr('click') + '}">' + this.title + '</button>';
    }
});

module.exports = FieldButton;