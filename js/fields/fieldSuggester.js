/**
 * Created by lukas on 11.6.15.
 */

'use strict';
var FieldInput = require('./fieldInput');
var Repository = require('../model/repository');
var dialogFactory = require('../dialogs/dialogFactory');
var TitleResolver = require('../utils/titleResolver');

var FieldSuggester = FieldInput.extend({

    repository: null,

    itemsDefinition: null,

    value: null,

    selectedTitle: null,

    add: null,

    titleResolver: null,

    /**
     *
     * @param config
     * @param parent
     */
    init: function(config, parent) {
        var self = this;
        this._super(config, parent);
        this.attr('itemsDefinition', this.config.find('items'));
        this.attr('repository', new Repository(this.itemsDefinition.attr('repository')));
        this.attr('add', this.config.attr('add'));
        this.titleResolver = new TitleResolver(this.itemsDefinition.find('title'));

        this.bind('value', function () {
            self.getFormConfig().entity.set(self.name, self.value);
        });
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        var html = '<div class="suggester">';
        html += '<div class="input-group">';
        html += '<input class="form-control" id="' + this.id + '" type="text" value="{{' + this.getTemplateAttr('selectedTitle') + '}}" />';
        html += '{{#if ' + this.getTemplateAttr('add') + '}}';
        html += '<div class="input-group-addon">';
        html += '<a href="#" can-click="' + this.getTemplateAttr('addItem') + '"><span class="glyphicon glyphicon-plus-sign"></span></a>';
        html += '</div>';
        html += '{{/if}}';
        html += '</div></div>';

        return html;
    },

    initValue: function () {
        var self = this;
        var entity = this.getFormConfig().entity;
        if (entity) {
            var itemId = entity.get(this.name);
            if (itemId) {
                this.repository.findOne(itemId).then(function(itemEntity){
                    self.attr('value', entity.get(self.name));
                    self.attr('selectedTitle', self.getTitle(itemEntity));
                });
            } else {
                self.attr('value', null);
                self.attr('selectedTitle', null);
            }

        }
    },


    initField: function() {
        var self = this;

        var textInput = $('#' + this.id);

        textInput.keyup(function () {
            $('#suggester').remove();
            if (textInput.val().length > 2) {
                self.repository.findLike(self.titleResolver.getFields(), textInput.val()).then(function (data) {
                    if (data.length == 1) {
                        self.attr('selectedTitle', self.getTitle(data[0]));
                        self.attr('value', data[0].id);
                        $('#suggester').remove();
                    } else if (data.length > 1) {
                        var suggester = $('<div />');
                        suggester.attr('id', 'suggester');

                        for (var i in data) {
                            var suggesterItem = $('<a />');
                            suggesterItem.text(self.getTitle(data[i]));
                            suggesterItem.attr('href', i);

                            suggesterItem.click(function () {
                                self.attr('value', data[$(this).attr('href')].id);
                                self.attr('selectedTitle', self.getTitle(data[$(this).attr('href')]));
                                $('#suggester').remove();
                                return false;
                            });
                            suggesterItem.appendTo(suggester);
                        }

                        textInput.after(suggester);
                    } else {
                        self.attr('value', null);
                        self.attr('selectedTitle', null);
                    }

                });
            }
        });
    },

    /**
     *
     * @param item
     * @param itemsDefinition
     * @returns {string}
     */
    getTitle: function(item) {
        return this.titleResolver.getTitle(item);
    },

    addItem: function() {
        var self = this;
        var entity = this.repository.getEmptyEntity();

        dialogFactory.showDialog('form', {
            module: this.add,
            entity: entity,
            repository: this.repository,
            sourceForm: this.getFormConfig()
        }).then(function (res) {
            if (res && res.id > 0) {
                self.attr('value', res.id);
                self.attr('selectedTitle', self.getTitle(res));
            }
        });
    }
});

module.exports = FieldSuggester;
