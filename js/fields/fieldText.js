/**
 * Created by lukas on 11.6.15.
 */

    'use strict';
    var FieldInput = require('./fieldInput');

    var FieldText = FieldInput.extend({
        value: null,

        init: function(config, parent) {
            var self = this;
            this._super(config, parent);

            this.bind('value', function () {
                self.getFormConfig().entity.set(self.name, self.value);
            });
        },

        /**
         *
         * @returns {string}
         */
        getFieldTemplate : function()
        {
            return '<input class="form-control" id="' + this.id + '" type="text" can-value="' + this.getTemplateAttr('value') + '" />';
        },

        initValue: function() {
            var value = '';
            var entity = this.getFormConfig().entity;
            if (entity) {
                value = entity.get(this.name) || '';
                this.attr('value', value);
            }
        }
    });

module.exports = FieldText;