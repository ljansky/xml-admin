/**
 * Created by lukas on 11.6.15.
 */

'use strict';
var Field = require('./field');

var FieldInput = Field.extend({
    name: null,
    inputValidator: null,
    error: null,
    defaultValue: null,

    init: function (config, parent) {
        this._super(config, parent);
        config = can.$(config);
        this.attr('name', config.attr('field'));

        if (typeof config.attr('default') !== 'undefined') {
            this.attr('defaultValue', config.attr('default'));
        }
    },

    getTemplate: function () {
        return '   <div class="form-group {{#if ' + this.getTemplateAttr('error') + '}}has-error has-feedback{{/if}}">' + this.getLabelTemplate() + this.getFieldTemplate()
            + '{{#if ' + this.getTemplateAttr('error') + '}}<span class="glyphicon glyphicon-alert form-control-feedback" aria-hidden="true"></span>{{/if}}'
            + '</div>';
    },

    getLabelTemplate: function () {
        return '<label for="{{' + this.getTemplateAttr('id') + '}}">{{' + this.getTemplateAttr('title') + '}}'
            + '{{#if ' + this.getTemplateAttr('error') + '}}: {{' + this.getTemplateAttr('error') + '}}{{/if}}'
            + '</label>';
    },

    setDefaultValue: function () {
        if (typeof this.parent.entity[this.name] === 'undefined' && this.defaultValue) {
            this.parent.entity.attr(this.name, this.defaultValue);
        }
    },

    setError: function (fieldName, message) {
        if (!fieldName || this.name === fieldName) {
            this.attr('error', message);
        }
    }
});

module.exports = FieldInput;
