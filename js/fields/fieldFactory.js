/**
 * Created by lukas on 29.6.15.
 */

'use strict';

var can = require('can');
var FieldText = require('./fieldText');
var FieldSelect = require('./fieldSelect');
var FieldCheckbox = require('./fieldCheckbox');
var FieldButton = require('./fieldButton');
var FieldCard = require('./fieldCard');
var FieldCards = require('./fieldCards');
var FieldRelated = require('./fieldRelated');
var FieldRelatedList = require('./fieldRelatedList');
var FieldForm = require('./fieldForm');
var FieldSuggester = require('./fieldSuggester');
var FieldActions = require('./fieldActions');
var FieldDate = require('./fieldDate');
var FieldLocation = require('./fieldLocation');
var FieldGrid = require('./fieldGrid');
var FieldWysiwyg = require('./fieldWysiwyg');
var FieldUploadImage = require('./fieldUploadImage');
var FieldEnum = require('./fieldEnum');
var FieldStaticText = require('./fieldStaticText');

var FieldFactory = {
    createField: function(config, parent) {
        var type = can.$(config).prop("tagName");
        switch (type) {
            case 'text':
                return new FieldText(config, parent);
            case 'date':
                return new FieldDate(config, parent);
            case 'select':
                return new FieldSelect(config, parent);
            case 'checkbox':
                return new FieldCheckbox(config, parent);
            case 'button':
                return new FieldButton(config, parent);
            case 'related-list':
                return new FieldRelatedList(config, parent);
            case 'card':
                return new FieldCard(config, parent, FieldFactory);
            case 'cards':
                return new FieldCards(config, parent, FieldFactory);
            case 'related':
                return new FieldRelated(config, parent, FieldFactory);
            case 'form':
                return new FieldForm(config, parent, FieldFactory);
            case 'suggester':
                return new FieldSuggester(config, parent);
            case 'actions':
                return new FieldActions(config, parent, FieldFactory);
            case 'location':
                return new FieldLocation(config, parent);
            case 'grid':
                return new FieldGrid(config, parent, FieldFactory);
            case 'wysiwyg':
                return new FieldWysiwyg(config, parent);
            case 'upload-image':
                return new FieldUploadImage(config, parent);
            case 'enum':
                return new FieldEnum(config, parent);
            case 'static-text':
                return new FieldStaticText(config, parent);
        }
        return new FieldText(config, parent);
    }
};

module.exports = FieldFactory;
