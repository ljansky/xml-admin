/**
 * Created by lukas on 28.9.15.
 */
'use strict';
var Field = require('./field');
var Geocoder = require('../utils/geocoder');

window.mapsLoaded = new can.Deferred();
window.mapsCallback = function() {
    window.mapsLoaded.resolve();
};

$.getScript('//maps.google.com/maps/api/js?sensor=false&callback=mapsCallback');

var FieldLocation = Field.extend({

    map: null,
    marker: null,

    init: function (config, parent) {
        this._super(config, parent);
        config = can.$(config);
        this.attr('latField', config.attr('lat'));
        this.attr('lngField', config.attr('lng'));
        this.attr('cityField', config.attr('city'));
        this.attr('addressField', config.attr('address'));
    },

    getTemplate: function () {
        return '   <div class="form-group {{#if ' + this.getTemplateAttr('error') + '}}has-error has-feedback{{/if}}">' + this.getLabelTemplate() + this.getFieldTemplate()
            + '</div>';
    },

    getLabelTemplate: function () {
        return '<label for="{{' + this.getTemplateAttr('id') + '}}">{{' + this.getTemplateAttr('title') + '}}'
            + '{{#if ' + this.getTemplateAttr('error') + '}}: {{' + this.getTemplateAttr('error') + '}}{{/if}}'
            + '</label>';
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate: function ()
    {
        return '<button type="button" can-click="' + this.getTemplateAttr('findAddress') + '" class="btn btn-info btn-xs">Najít adresu</button><div id="map-canvas" class="input-map"></div>';
    },

    initValue: function () {
        var self = this;
        window.mapsLoaded.then(function () {
            var location = self.getLocation();

            if (location.lat && location.lng)
            {
                self.placeMarker(new google.maps.LatLng(location.lat, location.lng), self.map);
                self.map.setCenter(new google.maps.LatLng(location.lat, location.lng));
            }
        });
    },

    initField: function () {
        var self = this;
        window.mapsLoaded.then(function () {
            var latlng = new google.maps.LatLng(50.087811, 14.42046);
            var mapOptions = {
                zoom: 12,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            self.map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
        });
    },

    placeMarker: function(location) {
        if (!this.marker) {
            this.marker = new google.maps.Marker({
                position: location,
                map: this.map
            });
        } else {
            this.marker.setPosition(location);
        }
    },

    findAddress: function() {
        var self = this;
        var geocoder = new Geocoder();
        console.log(this.city, this.address, this.entity);
        var address = this.getFormConfig().entity.get(this.cityField) + ', ' + this.getFormConfig().entity.get(this.addressField);
        geocoder.getLatLngByAdress(address)
            .done(function(latLng) {
                self.setLocation(latLng.lat(), latLng.lng());

                self.placeMarker(latLng);
                self.map.setCenter(latLng);
            });

        return false;
    },

    setLocation: function (lat, lng) {
        this.getFormConfig().entity.set('lat', lat);
        this.getFormConfig().entity.set('lng', lng);
    },

    getLocation: function () {
        return {
            lat: this.getFormConfig().entity.get('lat'),
            lng: this.getFormConfig().entity.get('lng')
        };
    }
});

module.exports = FieldLocation;