/**
 * Created by lukas on 10.11.15.
 */

'use strict';

var FieldInput = require('./fieldInput');
var Uploader = require('../model/uploader');

var FieldUploadImage = FieldInput.extend({
    /**
     *
     * @returns {string}
     */

    imageUrl: '',
    uploader: null,
    file: null,

    init: function (config, parent) {
        this._super(config, parent);
        this.attr('uploader', new Uploader('/api/upload/image', this.config.attr('directory')));
    },

    getFieldTemplate: function () {

        var html = '<div class="upload-image clearfix">'
        + '<img src="{{' + this.getTemplateAttr('imageUrl') + '}}" />'
        + '<div class="buttons">'
                + '<input can-change="' + this.getTemplateAttr('fileSelected') + '" type="file" />'
                + '<button can-click="' + this.getTemplateAttr('upload') + '">Upload</button>'
        + '</div>'
        + '</div>';
        return html;
    },

    upload: function () {
        var self = this;
        this.uploader.upload([this.file])
            .then(function (entity) {
                var formConfig = self.getFormConfig();
                console.log('setting', entity.id);
                formConfig.entity.set(self.name, entity.id);
                formConfig.callAction('save');
                self.setImage(entity.id);
            });

        return false;
    },

    fileSelected: function (env, input, c) {
        this.attr('file', input.context.files[0]);
        var imageUrl = window.URL.createObjectURL(this.file);
        this.attr('imageUrl', imageUrl);

        return false;
    },

    initValue: function () {
        console.log(this.name);
        var formConfig = this.getFormConfig();
        if (formConfig.entity && formConfig.entity.id > 0) {
            this.setImage(formConfig.entity.get(this.name));
        }
    },

    setImage: function (id) {
        console.log('set image', id);
        this.attr('imageUrl', '/api/image/' + id + '/1');
    }
});

module.exports = FieldUploadImage;