/**
 * Created by lukas on 31.7.15.
 */
'use strict';
require('../components/inlineForm');
var Field = require('./field');
var FieldForm = require('./fieldForm');

var Repository = require('../model/repository');

var FieldRelated = Field.extend({

    repository: null,
    items: null,
    fieldFactory: null,
    conf: null,
    hasValue: false,

    init: function(config, parent, FieldFactory) {
        //todo: vyresit conf
        this.attr('conf', config);
        this._super(config, parent);
        this.attr('repository', new Repository(this.config.attr('repository')));
        this.attr('items', new can.List([]));
        this.attr('fieldFactory', FieldFactory);
    },

    getTemplate: function () {
        var html = '<div class="form-group">';
        html += '{{#each ' + this.getTemplateAttr('items') + '}}';
        html += '<div class="related-item-wrap {{#if modified}}modified{{/if}}">';
        html += '<inline-form config={.}></inline-form>';
        html += '</div>';
        html += '{{/each}}';

        if (this.config.attr('add') === 'hidden') {
            html += '{{#if ' + this.getTemplateAttr('hasValue') + '}}';
            html += '<div class="related-item-wrap">';
            html += '<button type="button" can-click="' + this.getTemplateAttr('insertEmptyForm') + ' true" class="btn btn-info btn-xs">' + this.config.attr('add-label') + '</button>';
            html += '</div>';
            html += '{{/if}}';
        }

        html += '</div>';
        return html;
    },

    initValue: function () {
        var self = this;
        var formConfig = this.getFormConfig();

        if (formConfig.entity && formConfig.entity.id > 0) {
            this.attr('hasValue', true);
            this.repository.findRelated(this.config.attr('field'), formConfig.entity.id).then(function(data) {
                var items = [];
                data.forEach(function (entity) {
                    var subForm = new FieldForm(self.conf, null, self.fieldFactory, self);
                    items.push(subForm);
                    //todo: udelat promisu az jsou nacteny vsechny data a pak je nastavit
                    setTimeout(function () {
                        subForm.setData(entity);
                    }, 500);

                });

                self.items.replace(items);
                if (self.config.attr('add') !== 'hidden') {
                    self.insertEmptyForm();
                }
            });
        } else {
            this.items.replace([]);
        }
    },

    insertEmptyForm: function (force) {
        force = force || false;
        if (this.getFormConfig().entity.id && (force || this.config.attr('add') !== 'hidden')) {
            var newItem = this.repository.getEmptyEntity();
            newItem.set(this.config.attr('field'), this.getFormConfig().entity.id);

            this.items.push(new FieldForm(this.conf, newItem, this.fieldFactory, this));
        }
    },

    removeForm: function (id) {
        id = parseInt(id);
        var itemToRemove = null;
        this.items.each(function (item, index) {
            if (item.entity.id === id) {
                itemToRemove = index;
            }
        });

        this.items.splice(itemToRemove, 1);
    }
});

module.exports = FieldRelated;
