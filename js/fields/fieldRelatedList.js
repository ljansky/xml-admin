/**
 * Created by lukas on 22.9.15.
 */
'use strict';
require('../components/inlineForm');
var Field = require('./field');

var Repository = require('../model/repository');
var dialogFactory = require('../dialogs/dialogFactory');

var FieldRelatedList = Field.extend({

    repository: null,
    items: null,
    conf: null,

    init: function(config, parent) {
        this.attr('conf', config);
        this._super(config, parent);
        this.attr('repository', new Repository(this.config.attr('repository')));
        this.attr('items', new can.List([]));
    },

    getTemplate: function () {
        var html = '<div class="form-group">';

        html += '{{#if config.entity.data.id}}';

        html += '{{#each ' + this.getTemplateAttr('items') + '}}';
        html += '<div class="input-related-list-item">';
        html += '{{title}}';

        html += '<div class="related-item-list-buttons">';
        html += '<button type="button" can-click="' + this.getTemplateAttr('editItem') + ' entity" class="btn btn-info btn-xs">Upravit</button>';
        html += '<button type="button" can-click="' + this.getTemplateAttr('deleteItem') + ' entity" class="btn btn-danger btn-xs">Smazat</button>';
        html += '</div>';

        html += '</div>';
        html += '{{/each}}';

        html += '<div class="input-related-list-item">&nbsp;';
        html += '<div class="related-item-list-buttons">';
        html += '<button type="button" can-click="' + this.getTemplateAttr('addItem') + '" class="btn btn-info btn-xs">Přidat</button>';
        html += '</div>';
        html += '</div>';

        html += '{{/if}}';

        html += '</div>';
        return html;
    },

    initValue: function () {
        var self = this;
        var formConfig = this.getFormConfig();

        if (formConfig.entity && formConfig.entity.id > 0) {
            this.repository.findRelated(this.config.attr('field'), formConfig.entity.id).then(function(data) {
                var items = [];
                for (var i = 0; i < data.length; i++) {
                    items.push({title: self.getTitle(data[i]), entity: data[i]});
                }

                self.items.replace(items);
            });
        } else {
            this.items.replace([]);
        }
    },

    getTitle: function (entity) {
        //todo: dodelat title z xml
        return entity.get('title');
    },

    editItem: function (entity) {
        var self = this;
        dialogFactory.showDialog('form', {
            module: this.config.attr('module'),
            entity: entity,
            repository: this.repository
        }).then(function () {
            self.initValue();
        });

        return false;
    },

    deleteItem: function (entity) {
        var self = this;
        dialogFactory.showDialog('prompt', {
            message: 'Opravdu smazat?'
        }).then(function (yes) {
            if (yes) {
                entity.delete().then(function () {
                    self.initValue();
                });
            }
        });
        return false;
    },

    addItem: function () {
        var self = this;
        var entity = this.repository.getEmptyEntity();
        entity.set(this.config.attr('field'), this.getFormConfig().entity.id);
        dialogFactory.showDialog('form', {
            module: this.config.attr('module'),
            entity: entity,
            repository: this.repository
        }).then(function () {
            self.initValue();
        });

        return false;
    }
});

module.exports = FieldRelatedList;