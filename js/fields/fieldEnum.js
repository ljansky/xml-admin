/**
 * Created by lukas on 23.12.15.
 */

'use strict';
var FieldInput = require('./fieldInput');
var TitleResolver = require('../utils/titleResolver');
var Repository = require('../model/repository');

var FieldEnum = FieldInput.extend({
    repository: null,

    itemsDefinition: null,

    value: null,

    selectedTitle: null,

    titleResolver: null,

    /**
     *
     * @param config
     * @param parent
     */
    init: function(config, parent) {
        var self = this;
        this._super(config, parent);
        this.attr('itemsDefinition', this.config.find('items'));
        this.attr('repository', new Repository(this.itemsDefinition.attr('repository')));
        this.titleResolver = new TitleResolver(this.itemsDefinition.find('title'));
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        return '<p>{{' + this.getTemplateAttr('selectedTitle') + '}}</p> <input id="' + this.id + '" type="hidden" can-value="' + this.getTemplateAttr('value') + '" />';
    },

    initValue: function () {
        var self = this;
        var entity = this.getFormConfig().entity;
        if (entity) {
            var itemId = entity.get(this.name);
            if (itemId) {
                this.repository.findOne(itemId).then(function(itemEntity){
                    self.attr('value', entity.get(self.name));
                    self.attr('selectedTitle', self.getTitle(itemEntity));
                });
            } else {
                self.attr('value', null);
                self.attr('selectedTitle', null);
            }

        }
    },

    /**
     *
     * @param item
     * @param itemsDefinition
     * @returns {string}
     */
    getTitle: function(item) {
        return this.titleResolver.getTitle(item);
        var title = '';
        this.itemsDefinition.find('title field').each(function(){
            title += item.data[$(this).attr('name')]+' ';
        });
        return title.trim();
    }
});

module.exports = FieldEnum;