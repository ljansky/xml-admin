/**
 * Created by lukas on 13.8.15.
 */
'use strict';
var FieldContainer = require('./fieldContainer');

var FieldActions = FieldContainer.extend({

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        var html = '<div class="field-actions">';
        this.fields.each(function (field) {
            html += field.getTemplate();
        });
        html += '</div>';

        return html;
    }
});

module.exports = FieldActions;