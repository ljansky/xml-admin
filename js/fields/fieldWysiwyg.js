/**
 * Created by lukas on 1.11.15.
 */
'use strict';

var FieldInput = require('./fieldInput');

var FieldWysiwyg = FieldInput.extend({
    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        return '<textarea class="form-control" id="' + this.id + '" type="text" can-value="config.entity.data.' + this.name + '"></textarea>';
    }
});

module.exports = FieldWysiwyg;