/**
 * Created by lukas on 31.7.15.
 */
'use strict';
var FieldContainer = require('./fieldContainer');

var FieldCards = FieldContainer.extend({
    fields: null,

    init: function(config, parent, FieldFactory) {
        this._super(config, parent, FieldFactory);
        var first = true;
        this.fields.each(function (field) {
            if (first) {
                field.attr('active', true);
                first = false;
            }
        });
    },

    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        var html = '<ul class="nav nav-tabs">';
        this.fields.each(function (field) {
            html += field.getLabelTemplate();
        });
        html += '</ul>';

        html += '<div class="cards">';
        this.fields.each(function (field) {
            html += field.getFieldTemplate();
        });
        html += '</div>';

        return html;
    },

    hideCards: function () {
        this.fields.each(function (field) {
            field.attr('active', false);
        });
    }
});

module.exports = FieldCards;