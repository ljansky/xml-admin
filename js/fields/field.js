/**
 * Created by lukas on 22.6.15.
 */

'use strict';

var can = require('can');
require('can/construct/super');

var Field = can.Map.extend({
    counter: 0
}, {
    id: null,
    title: null,
    parent: null,
    initialized: null,
    config: null,
    disabled: false,

    init: function (config, parent) {
        this.attr('config', can.$(config));
        this.attr('parent', parent);
        this.attr('title', this.config.attr('title'));
        this.attr('id', 'Input' + Field.counter++);
        this.attr('disabled', this.config.attr('disabled') === 'new');
    },

    /**
     *
     * @returns {string}
     */
    getTemplateAttr: function (attr) {
        return this.parent.getFieldsPath() + this.id + '.' + attr;
    },

    getFieldTemplate: function () {
        return '';
    },

    getFormConfig: function () {
        return this.parent.getFormConfig();
    },

    getElement: function () {
        return $('#' + this.id);
    }
});

module.exports = Field;
