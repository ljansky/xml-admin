/**
 * Created by lukas on 19.6.15.
 */

    'use strict';

    var FieldInput = require('./fieldInput');

    var FieldCheckbox = FieldInput.extend({
        /**
         *
         * @returns {string}
         */
        getFieldTemplate : function()
        {
            return '<input type="checkbox"  id="' + this.id + '" can-change="{' + this.getTemplateAttr('click') +' config.entity}" {{#if config.entity.'+this.name+'}}checked="checked"{{/if}}" />';
        },

        click : function(entity) {
            entity.attr(this.name, !entity.attr(this.name));
        }
    });

module.exports = FieldCheckbox;