/**
 * Created by lukas on 22.9.15.
 */
'use strict';

var FieldInput = require('./fieldInput');
require('bootstrap-datepicker');
require('bootstrap-datepicker-locales/bootstrap-datepicker.cs');

var FieldDate = FieldInput.extend({
    /**
     *
     * @returns {string}
     */
    getFieldTemplate : function()
    {
        return '<div class="input-group date">'
            + '<input data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-autoclose="true" data-date-language="cs" class="form-control" id="' + this.id + '" type="text" can-value="config.entity.data.' + this.name + '" />'
            + '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>'
            + '</div>';
    }
});

module.exports = FieldDate;