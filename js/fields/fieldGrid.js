/**
 * Created by lukas on 28.9.15.
 */

'use strict';
var Field = require('./field');

var Repository = require('../model/repository');
var FieldForm = require('./fieldForm');
require('../components/gridForm');

var FieldGrid = Field.extend({

    repository: null,
    items: null,
    itemsMap: null,
    conf: null,
    options: null,
    itemsPromise: null,
    clickLocked: true,
    fieldFactory: null,
    formDef: null,

    init: function(config, parent, fieldFactory) {
        var self = this;
        this._super(config, parent);
        this.attr('repository', new Repository(this.config.attr('repository')));
        this.attr('items', new can.List([]));
        this.attr('itemsMap', new can.Map({}));
        this.attr('fieldFactory', fieldFactory);
        this.config.find('form').each(function (key, formDef) {
            self.attr('formDef', formDef);
        });
    },

    getTemplate: function () {
        var html = '<div class="form-group">'
        + '{{#each ' + this.getTemplateAttr('itemsMap') + '}}'
        + '<div class="grid-row">'
        + '<div class="grid-row-header"><div class="custom-checkbox{{#if checked}} checked{{/if}}" can-click="' + this.getTemplateAttr('updateItem') + ' ."></div> <label>{{itemTitle}}</label></div>'
        + '{{#if subForm}}<div class="grid-row-form"><grid-form config={subForm}></grid-form></div>{{/if}}'
        + '</div>'
        + '{{/each}}'
        + '</div>';

        return html;
    },

    initField: function () {
        this.itemsPromise = this.setItems();
    },

    initValue: function () {
        this.attr('clickLocked', true);
        if (this.itemsPromise) {
            var self = this;
            var formConfig = this.getFormConfig();
            if (formConfig.entity && formConfig.entity.id > 0) {
                this.itemsPromise.then(function () {
                    self.itemsMap.each(function (item) {
                        item.attr('checked', false);
                        item.removeAttr('subForm');
                    });
                    return self.repository.findRelated(self.config.attr('field'), formConfig.entity.id);
                }).then(function (relatedData) {
                    relatedData.forEach(function (relatedItem) {
                        self.checkItem(relatedItem);
                    });
                    self.attr('clickLocked', false);
                });
            }
        }
    },

    checkItem: function (item) {
        this.itemsMap[item.get(this.config.attr('item'))].attr('checked', true);
        if (this.formDef) {
            var subForm = new FieldForm(this.formDef, item, this.fieldFactory, this);
            this.itemsMap[item.get(this.config.attr('item'))].attr('subForm', subForm);
        }
    },

    updateItem: function (item) {
        if (!this.clickLocked) {
            this.attr('clickLocked', true);
            var self = this;
            var formConfig = this.getFormConfig();
            if (this.itemsMap[item.itemId].checked) {
                var findOptions = {
                    where: {}
                };
                findOptions.where[this.config.attr('field')] = formConfig.entity.id;
                findOptions.where[this.config.attr('item')] = item.itemId;
                return this.repository.findAll(findOptions).then(function (entities) {
                    if (entities.length === 1) {
                        return entities[0].delete();
                    }

                }).then(function (res) {
                    self.itemsMap[item.itemId].attr('checked', false);
                    self.itemsMap[item.itemId].removeAttr('subForm');
                    self.attr('clickLocked', false);
                    return res;
                });

            } else {
                this.itemsMap[item.itemId].attr('checked', true);
                var newEntity = this.repository.getEmptyEntity();
                newEntity.set(this.config.attr('field'), formConfig.entity.id);
                newEntity.set(this.config.attr('item'), item.itemId);
                return newEntity.saveNew().then(function (res) {
                    self.itemsMap[item.itemId].attr('checked', true);
                    var subForm = new FieldForm(self.formDef, res, self.fieldFactory, self);
                    self.itemsMap[item.itemId].attr('subForm', subForm);
                    self.attr('clickLocked', false);
                    return res;
                });
            }
        }
    },

    setItems: function() {
        var deferred = new can.Deferred();
        var self = this;
        this.itemsDefinition = this.config.find('items');

        var repository = new Repository(this.itemsDefinition.attr('repository'));

        repository.findAll({}).then(function (data) {
            var items = [];
            var map = {};
            for (var i in data) {
                var item = {itemId: data[i].id, itemTitle: self.getTitle(data[i]), checked: false};
                items.push(item);
                map[item.itemId] = item;
            }
            self.items.replace(items);
            self.itemsMap.attr(map, true);
            deferred.resolve();
        });

        return deferred;
    },

    getTitle: function(item) {
        var title = '';
        this.itemsDefinition.find('title field').each(function(){
            title += item.get($(this).attr('name'))+' ';
        });
        return title.trim();
    }
});

module.exports = FieldGrid;