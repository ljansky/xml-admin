/**
 * Created by lukas on 3.8.15.
 */
/**
 * Created by lukas on 31.7.15.
 */
'use strict';
var Field = require('./field');

var FieldContainer = Field.extend({
    fields: null,

    init: function(config, parent, FieldFactory) {
        var self = this;
        this._super(config, parent);
        this.attr('fields', new can.Map());

        can.$(config).children().each(function (key, field) {
            if (!self.getFormConfig().formId || typeof $(field).attr('popup') === 'undefined' || $(field).attr('popup') !== 'false') {
                var newField = FieldFactory.createField(field, self);
                self.fields.attr(newField.id, newField);
            }
        });
    },

    getLabelTemplate: function () {
        return '';
    },

    getTemplate: function () {
        return this.getFieldTemplate();
    },

    getFieldsPath: function () {
        return this.parent.getFieldsPath() + this.id + '.fields.';
    },

    initField: function () {
        this.fields.each(function (field) {
            if (typeof field.initField === 'function') {
                field.initField();
            }
        });
    },

    initValue: function () {
        this.attr('disabled', !this.getFormConfig().entity.id && this.config.attr('disabled') === 'new');

        this.fields.each(function (field) {
            if (typeof field.initValue === 'function') {
                field.initValue();
            }
        });
    },

    setError: function (fieldName, message) {
        this.fields.each(function (field) {
            if (typeof field.setError === 'function') {
                field.setError(fieldName, message);
            }
        });
    }
});

module.exports = FieldContainer;