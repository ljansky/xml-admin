/**
 * Created by lukas on 14.10.15.
 */

var dialogFactory = require('../dialogs/dialogFactory');

var actionSave = function (closeAfter) {

    if (typeof closeAfter === 'undefined') {
        closeAfter = true;
    }

    var self = this;
    if (this.entity.id > 0) {
        return this.entity.save().then(function (entity) {

            self.dispatch('saved', [entity]);
            self.setFlashMessage('Záznam byl uložen.');

            if (self.formId) {
                dialogFactory.closeDialog(self.formId);
            }

            return {
                insert: false,
                entity: entity
            }
        });
    } else {
        return this.entity.saveNew().then(function (entity) {

            self.dispatch('saved', [entity]);
            self.setData(entity);
            self.setFlashMessage('Záznam byl vytvořen.');

            if (self.formId && closeAfter) {
                dialogFactory.closeDialog(self.formId);
            }

            return {
                insert: true,
                entity: entity
            }
        });
    }
};

module.exports = actionSave;