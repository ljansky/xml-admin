/**
 * Created by lukas on 14.10.15.
 */

var actionNew = function () {
    var entity = this.repository.getEmptyEntity();
    this.setData(entity);
    return new can.Deferred().resolve();
};

module.exports = actionNew;