/**
 * Created by lukas on 14.10.15.
 */

var dialogFactory = require('../dialogs/dialogFactory');

var actionDelete = function () {
    var self = this;
    return dialogFactory.showDialog('prompt', {
        message: 'Opravdu smazat?'
    }).then(function (res) {
        if (res) {
            return self.entity.delete();
        } else {
            return null;
        }
    }).then(function (res) {

        if (res && self instanceof window.FormConfig) {
            self.setData(self.repository.getEmptyEntity());
            self.dispatch('deleted', []);
        }

        if (res) {
            self.setFlashMessage('Záznam byl smazán.');
        }

        return res;
    });
};

module.exports = actionDelete;