/**
 * Created by lukas on 14.10.15.
 */

var actionDelete = require('./actionDelete');
var actionNew = require('./actionNew');
var actionSave = require('./actionSave');

var actionFactory = {

    getAction: function (name) {
        switch (name) {
            case 'save':
                return actionSave;
            case 'new':
                return actionNew;
            case 'delete':
                return actionDelete;
            default:
                return null;
        }
    }

};

module.exports = actionFactory;